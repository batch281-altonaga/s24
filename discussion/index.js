// Exponent operator

// using Math.pow()

// Template literals

// Array destructuring
/* 
    - allows us to unpack elements in arrays into distinct variables
    - allows us to name array elements with variables instead of using index numbers
    Syntax:
        let/const [variableName, variableName, variableName] = array
*/

const fullName = ["Juan", "Castro", "Dela Cruz"];

// Pre-Array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`
);

// Array destructuring
const [firstName, middleName, lastName] = fullName;

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`
);

// Object destructuring
/* 
    - allows us to unpack properties of objects into distinct variables
    Syntax:
        let/const {propertyName, propertyName, propertyName} = object;    
*/

const person = {
  givenName: "Jane",
  maidenName: "Castro",
  familyName: "Dela Cruz",
};

// Pre-object destructing
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`
);

// Object destructing
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

// Arrow functions
/* 
    - Compact alternative syntax to traditional functions
    Syntax:
        const variableName = () => {
            console.log();
        }
*/

const hello = () => {
  console.log("Hello, world!");
};

// Pre-arrow function and template literals
/* 
    function printFullName (firstName, middleInitial, lastName){
        console(firstName + " " + middleInitial + " " + lastName);
    };
    printFullName("John", "D", "Smith");
*/

// Implementation of Arrow Function
const printFullName = (firstName, middleInitial, lastName) => {
  console.log(`${firstName} ${middleInitial} ${lastName}`);
};

printFullName("John", "D", "Smith");

// Arrow functions with loops
// Pre-arrow function
const students = ["John", "Jane", "Joy"];
students.forEach(function (student) {
  console.log(`${student} is a student`);
});

// Arrow functions in loops
students.forEach((student) => {
  console.log(`${student} is a student`);
});

// Implicit return statement
// Pre-arrow function
/* 
    const add = (x,y) => {
        return x+y;
    };

    let total = add(1,2);
    console.log(total);
*/

// Implicit return
const add = (x, y) => x + y;
let total = add(1, 2);
console.log(total);

// Default function argument value

const greet = (name = "User") => {
  return `Good morning, ${name}!`;
};

console.log(greet("Ray"));

// Class based object blueprints

// Creating a class

class Car {
  construct(brand, name, year) {
    (this.brand = brand), (this.name = name), (this.year = year);
  }
}

// Instantiating an object

const myCar = new Car();

console.log(myCar);

// Values of properties assigned after instantiation of an object

myCar.brand = "ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// Instantiate a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);
